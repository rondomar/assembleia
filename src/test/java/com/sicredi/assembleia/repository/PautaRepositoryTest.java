package com.sicredi.assembleia.repository;

import com.sicredi.assembleia.dto.PautaRecordDto;
import com.sicredi.assembleia.model.PautaModel;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ActiveProfiles("test")
class PautaRepositoryTest {

    @Autowired
    PautaRepository pautaRepository;

    @Autowired
    EntityManager entityManager;

    @Test
    @DisplayName("Obter com sucesso um usuário do banco de dados.")
    void findByNomePautaSuccess() {
        String nomePauta = "Assembléias 2024";
        PautaRecordDto pautaRecordDto = new PautaRecordDto(null, null,
                nomePauta, 1) ;
        this.criarPauta(pautaRecordDto);

        Optional<PautaModel> pautaExiste = this.pautaRepository.findByNomePauta(nomePauta);

        assertThat(pautaExiste.isPresent()).isTrue();
    }

    public PautaModel criarPauta(PautaRecordDto pautaRecordDto){

        PautaModel novaPautaModel = new PautaModel();
        BeanUtils.copyProperties(pautaRecordDto, novaPautaModel);
        this.entityManager.persist(novaPautaModel);

        return novaPautaModel;
    }
}