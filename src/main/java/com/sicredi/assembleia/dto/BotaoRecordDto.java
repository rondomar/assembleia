package com.sicredi.assembleia.dto;

public record BotaoRecordDto(String texto, String url) {
}
