package com.sicredi.assembleia.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public record PautaRecordDto (
        @JsonProperty("id") UUID idPauta,
        @JsonProperty("idassociado") UUID idAssociado,
        @JsonProperty("pauta") String nomePauta,
        @JsonProperty("tempovotacao") int tempoVotacao) {
}
