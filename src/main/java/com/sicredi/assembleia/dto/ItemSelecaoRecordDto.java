package com.sicredi.assembleia.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public record ItemSelecaoRecordDto(
        String texto,
        String url
) {

    public ItemSelecaoRecordDto(String texto) {
        this(texto, null);
    }
}
