package com.sicredi.assembleia.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public record VotacaoRecordDto(
        @JsonProperty("id") UUID id,
        @JsonProperty("idassociado") UUID idAssociado,
        @JsonProperty("idpauta") UUID idPauta,
        @JsonProperty("voto") Boolean voto
        ) {

    public boolean isVoto(){
        return true;
    }
}