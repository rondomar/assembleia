package com.sicredi.assembleia.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public record InicioRecordDto(
   String tipo,
   String titulo,
   List<ItemInicioRecordDto> itens,
   BotaoRecordDto botaoOk,
   BotaoRecordDto botaoCancelar
) {}