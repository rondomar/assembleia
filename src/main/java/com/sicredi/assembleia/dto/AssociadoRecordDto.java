package com.sicredi.assembleia.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigInteger;
import java.util.UUID;

public record AssociadoRecordDto(
        @JsonProperty("id") UUID idAssociado,
        @JsonProperty("nome") String nomeAssociado,
        @JsonProperty("cpf") BigInteger numeroCpfAssociado) {
}
