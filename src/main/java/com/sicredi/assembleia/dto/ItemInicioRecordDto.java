package com.sicredi.assembleia.dto;

import jakarta.validation.constraints.Null;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public record ItemInicioRecordDto(
        String tipo,
        @Null String texto,
        @Null String id,
        @Null String titulo,
        @Null String valor,
        @Null String url,
        @Null ItemSelecaoRecordDto itemSelecao
) {

    public ItemInicioRecordDto(String tipo, String texto) {
        this(tipo, texto, null, null, null, null, null);
    }

    public ItemInicioRecordDto(ItemSelecaoRecordDto itemSelecaoRecordDto) {

        this(null, itemSelecaoRecordDto.texto(), null, null, null, itemSelecaoRecordDto.url(), null);
    }

    public ItemInicioRecordDto(String tipo, String id, String titulo, String valor) {
        this(tipo,null, id, titulo, valor, null, null);
    }
}
