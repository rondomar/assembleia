package com.sicredi.assembleia.model;

import com.sicredi.assembleia.dto.PautaRecordDto;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "tb_pauta")
@AllArgsConstructor
@NoArgsConstructor
public class PautaModel implements Serializable {

    private static final long serialVersionUI = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID idPauta;

    private String nomePauta;

    private int tempoVotacao = 1;

    public UUID getIdPauta() {
        return idPauta;
    }

    public void setIdPauta(UUID idPauta) {
        this.idPauta = idPauta;
    }

    public String getNomePauta() {
        return nomePauta;
    }

    public void setNomePauta(String nomePauta) {
        this.nomePauta = nomePauta;
    }

    public int getTempoVotacao() {
        return tempoVotacao;
    }

    public void setTempoVotacao(int tempoVotacao) {
        if (tempoVotacao < 1 || tempoVotacao > 60) {
            throw new IllegalArgumentException("Tempo da votação deve ser entre 1 e 60.");
        }
        this.tempoVotacao = tempoVotacao;
    }

    @Override
    public String toString() {
        return "PautaModel{" +
                "idPauta=" + idPauta +
                ", nomePauta='" + nomePauta + '\'' +
                ", tempoVotacao=" + tempoVotacao +
                '}';
    }
}
