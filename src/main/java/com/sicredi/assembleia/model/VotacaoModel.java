package com.sicredi.assembleia.model;

import com.sicredi.assembleia.dto.VotacaoRecordDto;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "tb_votacao")
@AllArgsConstructor
@NoArgsConstructor
public class VotacaoModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID idVotacao;

    @ManyToOne
    @JoinColumn(name = "id_associado", nullable = false)
    private AssociadoModel idAssociado;

    @ManyToOne
    @JoinColumn(name = "id_pauta", nullable = false)
    private PautaModel idPauta;

    private Boolean voto;

    public UUID getIdVotacao() {
        return idVotacao;
    }

    public void setIdVotacao(UUID idVotacao) {
        this.idVotacao = idVotacao;
    }

    public AssociadoModel getIdAssociado() {
        return idAssociado;
    }

    public void setIdAssociado(AssociadoModel idAssociado) {
        this.idAssociado = idAssociado;
    }

    public PautaModel getIdPauta() {
        return idPauta;
    }

    public void setIdPauta(PautaModel idPauta) {
        this.idPauta = idPauta;
    }

    public Boolean getVoto() {
        return voto;
    }

    public void setVoto(Boolean voto) {
        this.voto = voto;
    }

    @Override
    public String toString() {
        return "VotacaoModel{" +
                "idVotacao=" + idVotacao +
                ", associado=" + idAssociado +
                ", pauta=" + idPauta +
                ", voto=" + voto +
                '}';
    }

}
