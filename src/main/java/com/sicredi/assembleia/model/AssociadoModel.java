package com.sicredi.assembleia.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.UUID;

@Entity
@Table(name = "tb_associado")
public class AssociadoModel implements Serializable {

    private static final long serialVersionUI = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID idAssociado;

    private String nomeAssociado;

    private BigInteger numeroCpfAssociado;

    public UUID getIdAssociado() {
        return idAssociado;
    }

    public void setIdAssociado(UUID idAssociado) {
        this.idAssociado = idAssociado;
    }

    public String getNomeAssociado() {
        return nomeAssociado;
    }

    public void setNomeAssociado(String nomeAssociado) {
        this.nomeAssociado = nomeAssociado;
    }

    public BigInteger getNumeroCpfAssociado() {
        return numeroCpfAssociado;
    }

    public void setNumeroCpfAssociado(BigInteger numeroCpfAssociado) {
        this.numeroCpfAssociado = numeroCpfAssociado;
    }

    @Override
    public String toString() {
        return "AssociadoModel{" +
                "idAssociado=" + idAssociado +
                ", nomeAssociado='" + nomeAssociado + '\'' +
                ", numeroCpfAssociado=" + numeroCpfAssociado +
                '}';
    }
}
