package com.sicredi.assembleia.controller;

import com.sicredi.assembleia.config.AppConfig;
import com.sicredi.assembleia.dto.*;
import com.sicredi.assembleia.model.PautaModel;
import com.sicredi.assembleia.repository.PautaRepository;
import com.sicredi.assembleia.util.FormularioApp;
import com.sicredi.assembleia.util.MsgApp;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class VotacaoController {

    private final PautaRepository pautaRepository;
    private final BotaoRecordDto botaoRecordDtoCancelar;
    private final AppConfig appConfig;
    private final FormularioApp formularioApp;

    @Autowired
    public VotacaoController(PautaRepository pautaRepository,
                             AppConfig appConfig,
                             FormularioApp formularioApp) {
        this.pautaRepository = pautaRepository;
        this.appConfig = appConfig;
        this.formularioApp = formularioApp;
        this.botaoRecordDtoCancelar = new BotaoRecordDto(appConfig.getNomeBotaoCancelar(), appConfig.getBaseUrlInicio());
    }

    @PostMapping("/votacao-listar")
    public ResponseEntity<InicioRecordDto> votacaoListar() {
        List<PautaModel> pautaModelList = pautaRepository.findAll();
        String titulo = "Listar votação";

        if (pautaModelList.isEmpty()) {
            return construirRespostaFalha(titulo, MsgApp.MSG0007);
        }

        return construirRespostaSucesso(titulo);
    }


    @PostMapping("/votacao-cadastrar")
    public ResponseEntity<InicioRecordDto> votacaoCadastrar(@RequestBody @Valid VotacaoRecordDto votacaoRecordDto) {

        String titulo = "Votação";
        Optional<PautaModel> pautaModel = pautaRepository.findById(votacaoRecordDto.idPauta());

        if(pautaModel.isEmpty()){
            return construirRespostaFalha(titulo, MsgApp.MSG0010);
        }

        List<ItemInicioRecordDto> itens = List.of(
                new ItemInicioRecordDto("TEXTO", "Pauta: " + pautaModel.get().getNomePauta()),
                new ItemInicioRecordDto("TEXTO", "Tempo de pauta: "+pautaModel.get().getTempoVotacao()+" minuto(s)."),
                new ItemInicioRecordDto("INPUT_TEXTO", "voto","Sim/Não", null)
        );

        BotaoRecordDto botaoRecordDtoOk = new BotaoRecordDto("VOTAR", appConfig.getBaseUrlVotacaoCadastrarNova());

        return this.formularioApp.formulario(titulo, itens, botaoRecordDtoOk, this.botaoRecordDtoCancelar);
    }

    private ResponseEntity<InicioRecordDto> construirRespostaSucesso(String titulo) {

        List<ItemInicioRecordDto> itens = pautaRepository.findAll().stream()
                .map(pautaModel -> new ItemInicioRecordDto(
                        pautaModel.getNomePauta(),
                        appConfig.getBaseUrlVotacaoCadastrar() + "/" + pautaModel.getIdPauta()
                ))
                .collect(Collectors.toList());

        return this.formularioApp.selecao(titulo, itens, null, this.botaoRecordDtoCancelar);
    }

    private ResponseEntity<InicioRecordDto> construirRespostaFalha(String titulo, String mensagem) {

        List<ItemInicioRecordDto> itens = List.of(
                new ItemInicioRecordDto("TEXTO", mensagem)
        );

        return this.formularioApp.formulario(titulo, itens, null, this.botaoRecordDtoCancelar);
    }
}
