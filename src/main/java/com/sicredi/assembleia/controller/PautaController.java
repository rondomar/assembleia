package com.sicredi.assembleia.controller;

import com.sicredi.assembleia.config.AppConfig;
import com.sicredi.assembleia.dto.*;
import com.sicredi.assembleia.model.AssociadoModel;
import com.sicredi.assembleia.model.PautaModel;
import com.sicredi.assembleia.repository.AssociadoRepository;
import com.sicredi.assembleia.repository.PautaRepository;
import com.sicredi.assembleia.repository.VotacaoRepository;
import com.sicredi.assembleia.util.FormularioApp;
import com.sicredi.assembleia.util.MsgApp;
import jakarta.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.List;
import java.util.stream.Stream;

@RestController
public class PautaController {

    @Autowired
    PautaRepository pautaRepository;

    private final BotaoRecordDto botaoRecordDtoCancelar;
    private final VotacaoRepository votacaoRepository;
    private final AssociadoRepository associadoRepository;
    private final AppConfig appConfig;
    private final FormularioApp formularioApp;

    public PautaController(AppConfig appConfig,
                           FormularioApp formularioApp,
                           VotacaoRepository votacaoRepository,
                           AssociadoRepository associadoRepository) {
        this.appConfig = appConfig;
        this.formularioApp = formularioApp;
        this.votacaoRepository = votacaoRepository;
        this.associadoRepository = associadoRepository;
        this.botaoRecordDtoCancelar = new BotaoRecordDto(appConfig.getNomeBotaoCancelar(), appConfig.getBaseUrlInicio());
    }

    @PostMapping("/pauta")
    public ResponseEntity<PautaModel> pautaNova(@RequestBody @Valid PautaRecordDto pautaRecordDto) {
        var pautaModel = new PautaModel();
        BeanUtils.copyProperties(pautaRecordDto, pautaModel);
        return ResponseEntity.status(HttpStatus.CREATED).body(pautaRepository.save(pautaModel));
    }

    @PostMapping("/pauta-cadastrar-nova")
    public ResponseEntity<InicioRecordDto> pautaCadastrarNova(@RequestBody @Valid PautaRecordDto pautaRecordDto) {
        String titulo = "Cadastrar pauta";

        var pautaModel = new PautaModel();
        BeanUtils.copyProperties(pautaRecordDto, pautaModel);
        pautaRepository.save(pautaModel);

        VotacaoRecordDto votacaoRecordDto = new VotacaoRecordDto(null,
                pautaRecordDto.idAssociado(), pautaModel.getIdPauta(), null);

        return pautaDetalhamento(titulo, votacaoRecordDto);
    }

    @PostMapping("/pauta-cadastrar")
    public ResponseEntity<InicioRecordDto> pautaCadastrar() {

        List<ItemInicioRecordDto> itens = List.of(
                new ItemInicioRecordDto("INPUT_TEXTO", "nomePauta",
                        "Pauta", null),
                new ItemInicioRecordDto("INPUT_TEXTO", "tempoPauta",
                        "Tempo de votação (em minutos)", null)
        );

        BotaoRecordDto botaoRecordDtoOk = new BotaoRecordDto("Acessar", appConfig.getBaseUrlPautaCadastrarNova());

        return this.formularioApp.formulario("Cadastrar pauta",
                itens, botaoRecordDtoOk, this.botaoRecordDtoCancelar);
    }

    @PostMapping("/pauta-detalhar")
    public ResponseEntity<InicioRecordDto> pautaDetalhar(@RequestBody @Valid PautaRecordDto pautaRecordDto) {

        String titulo = "Pauta";
        VotacaoRecordDto votacaoRecordDto = new VotacaoRecordDto(null,
                pautaRecordDto.idAssociado(), pautaRecordDto.idPauta(), null);
        return pautaDetalhamento(titulo, votacaoRecordDto);
    }

    @PostMapping("/pauta-listar")
    public ResponseEntity<InicioRecordDto> pautaListar() {

        List<PautaModel> pautaModelList = pautaRepository.findAll();
        String titulo = "Listar pauta(s)";

        if (pautaModelList.isEmpty()) {
            return construirRespostaFalha(titulo, MsgApp.MSG0007);
        }

        return construirRespostaSucesso(titulo);
    }

    public ResponseEntity<InicioRecordDto> pautaDetalhamento(String titulo, VotacaoRecordDto votacaoRecordDto) {

        List<PautaModel> pautaModelList = pautaRepository.findById(votacaoRecordDto.idPauta()).stream().toList();
        if(pautaModelList.isEmpty()){
            return construirRespostaFalha(titulo, MsgApp.MSG0008);
        }

        Optional<AssociadoModel> associadoModel = associadoRepository.findById(votacaoRecordDto.idAssociado());
        if(associadoModel.isEmpty()){
            return construirRespostaFalha(titulo, MsgApp.MSG0009);
        }

        return construirRespostaPautaSucesso(titulo, pautaModelList, associadoModel.get());
    }

    private ResponseEntity<InicioRecordDto> construirRespostaPautaSucesso(String titulo,
                                                                          List<PautaModel> pautaModelList,
                                                                          AssociadoModel associadoModel) {
        List<ItemInicioRecordDto> itens = pautaModelList.stream()
                .flatMap(pauta -> Stream.of(
                        new ItemInicioRecordDto("TEXTO", pauta.getNomePauta()),
                        new ItemInicioRecordDto("TEXTO", "Tempo da pauta: "+pauta.getTempoVotacao()+" minuto(s)."),
                        new ItemInicioRecordDto("TEXTO", "Total de votos SIM: "+
                                votacaoRepository.countByIdPautaAndIdAssociadoAndVoto(pauta, associadoModel, Boolean.TRUE)),
                        new ItemInicioRecordDto("TEXTO", "Total de votos NÃO: "+
                                votacaoRepository.countByIdPautaAndIdAssociadoAndVoto(pauta, associadoModel, Boolean.FALSE))
                ))
                .collect(Collectors.toList());

        return this.formularioApp.formulario(titulo, itens, null, this.botaoRecordDtoCancelar);
    }

    private ResponseEntity<InicioRecordDto> construirRespostaSucesso(String titulo) {

        List<ItemInicioRecordDto> itens = pautaRepository.findAll().stream()
                .map(pautaModel -> new ItemInicioRecordDto(
                        pautaModel.getNomePauta(),
                        appConfig.getBaseUrlPautaDetalhar() + "/" + pautaModel.getIdPauta()
                ))
                .collect(Collectors.toList());

        return this.formularioApp.selecao(titulo, itens, null, this.botaoRecordDtoCancelar);
    }

    private ResponseEntity<InicioRecordDto> construirRespostaFalha(String titulo, String mensagem) {

        List<ItemInicioRecordDto> itens = List.of(
                new ItemInicioRecordDto("TEXTO", mensagem)
        );

        return this.formularioApp.formulario(titulo, itens, null, this.botaoRecordDtoCancelar);
    }
}
