package com.sicredi.assembleia.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sicredi.assembleia.config.AppConfig;
import com.sicredi.assembleia.dto.*;
import com.sicredi.assembleia.model.AssociadoModel;
import com.sicredi.assembleia.repository.AssociadoRepository;
import com.sicredi.assembleia.service.CpfValidationService;
import com.sicredi.assembleia.util.FormularioApp;
import com.sicredi.assembleia.util.MsgApp;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class InicioController {

    private final ObjectMapper objectMapper;
    private final CpfValidationService cpfValidationService;
    private final AssociadoRepository associadoRepository;
    private final BotaoRecordDto botaoRecordDtoCancelar;
    private final AppConfig appConfig;
    private final FormularioApp formularioApp;

    public InicioController(ObjectMapper objectMapper,
                            CpfValidationService cpfValidationService,
                            AssociadoRepository associadoRepository,
                            AppConfig appConfig,
                            FormularioApp formularioApp) {
        this.objectMapper = objectMapper;
        this.cpfValidationService = cpfValidationService;
        this.associadoRepository = associadoRepository;
        this.appConfig = appConfig;
        this.formularioApp = formularioApp;
        this.botaoRecordDtoCancelar = new BotaoRecordDto(appConfig.getNomeBotaoCancelar(), appConfig.getBaseUrlInicio());
    }

    @GetMapping("/inicio")
    public ResponseEntity<InicioRecordDto> obterInicio() {
        return construirRespostaInicio(MsgApp.MSG0001);
    }

    @PostMapping("/inicio")
    public ResponseEntity<InicioRecordDto> criarInicio(@RequestBody @Valid AssociadoRecordDto associadoRecordDto) {

        // Parâmetro falso adicionado, link proposto no teste não funciona
        boolean isValid = cpfValidationService.isValidCpf(associadoRecordDto.numeroCpfAssociado().toString(), false);

        if (isValid) {

            Optional<AssociadoModel> associado = associadoRepository.findByNumeroCpfAssociado(
                    new BigInteger(associadoRecordDto.numeroCpfAssociado().toString()));

            if(associado.isPresent()){
                return construirRespostaFuncionalidade();
            }

            return construirRespostaInicio(MsgApp.MSG0001);
        } else {
            return construirRespostaInicio(MsgApp.MSG0005);
        }
    }

    private ResponseEntity<InicioRecordDto> construirRespostaInicio(String mensagem) {

        List<ItemInicioRecordDto> itens = List.of(
                new ItemInicioRecordDto("TEXTO", mensagem),
                new ItemInicioRecordDto("INPUT_TEXTO", "idCPFTexto", "CPF", "161.817.310-32")
        );

        BotaoRecordDto botaoRecordDtoOk = new BotaoRecordDto("Acessar", appConfig.getBaseUrlInicio());

        return this.formularioApp.formulario("CASA STARK",
                itens, botaoRecordDtoOk, this.botaoRecordDtoCancelar);
    }

    private ResponseEntity<InicioRecordDto> construirRespostaFuncionalidade() {

        List<ItemSelecaoRecordDto> itensSelecao = List.of(
                new ItemSelecaoRecordDto("Listar pautas", appConfig.getBaseUrlPautaListar()),
                new ItemSelecaoRecordDto("Cadastrar pauta", appConfig.getBaseUrlPautaCadastrar()),
                new ItemSelecaoRecordDto("Listar votação", appConfig.getBaseUrlVotacaoListar()),
                new ItemSelecaoRecordDto("Cadastrar votação", appConfig.getBaseUrlVotacaoCadastrar())
        );

        List<ItemInicioRecordDto> itens = new ArrayList<>();
        itensSelecao.forEach(item -> {
            itens.add(new ItemInicioRecordDto(item.texto(), item.url()));
        });

        return this.formularioApp.selecao("Funcionalidades",
                                        itens, null, this.botaoRecordDtoCancelar);
    }
}
