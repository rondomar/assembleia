package com.sicredi.assembleia.controller;

import com.sicredi.assembleia.dto.AssociadoRecordDto;
import com.sicredi.assembleia.model.AssociadoModel;
import com.sicredi.assembleia.repository.AssociadoRepository;
import jakarta.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AssociadoController {

    @Autowired
    AssociadoRepository associadoRepository;

    @PostMapping("/associado")
    public ResponseEntity<AssociadoModel> saveAssociado(@RequestBody @Valid AssociadoRecordDto associadoRecordDto) {
        var associadoModel = new AssociadoModel();
        BeanUtils.copyProperties(associadoRecordDto, associadoModel);
        return ResponseEntity.status(HttpStatus.CREATED).body(associadoRepository.save(associadoModel));
    }
}
