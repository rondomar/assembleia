package com.sicredi.assembleia.repository;

import com.sicredi.assembleia.model.PautaModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface PautaRepository extends JpaRepository<PautaModel, UUID> {

    Optional<PautaModel> findByNomePauta(String nomePauta);
}
