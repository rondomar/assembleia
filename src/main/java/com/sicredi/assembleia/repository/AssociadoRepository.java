package com.sicredi.assembleia.repository;

import com.sicredi.assembleia.model.AssociadoModel;
import com.sicredi.assembleia.model.PautaModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface AssociadoRepository extends JpaRepository<AssociadoModel, UUID> {

    Optional<AssociadoModel> findByNumeroCpfAssociado(BigInteger numeroCpfAssociado);

    interface VotacaoRepository  extends JpaRepository<PautaModel, UUID> {
    }
}
