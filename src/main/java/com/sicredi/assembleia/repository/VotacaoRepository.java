package com.sicredi.assembleia.repository;

import com.sicredi.assembleia.model.AssociadoModel;
import com.sicredi.assembleia.model.PautaModel;
import com.sicredi.assembleia.model.VotacaoModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface VotacaoRepository extends JpaRepository<VotacaoModel, UUID> {

    long countByIdPautaAndIdAssociadoAndVoto(PautaModel idPauta, AssociadoModel idAssociado, Boolean voto);
}

