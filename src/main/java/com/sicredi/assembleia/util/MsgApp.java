package com.sicredi.assembleia.util;

public class MsgApp {

    public static final String MSG0001 = "Bem-vindo!";
    public static final String MSG0002 = "CPF válido.";
    public static final String MSG0003 = "CPF inválido.";
    public static final String MSG0004 = "Ocorreu um erro.";
    public static final String MSG0005 = MsgApp.MSG0003 + " Tente novamente!";
    public static final String MSG0006 = "Vote com sabedoria.";
    public static final String MSG0007 = "Nenhuma pauta encontrada.";
    public static final String MSG0008 = "Não foi possível abrir a pauta.";
    public static final String MSG0009 = "Código do associado inválido.";
    public static final String MSG0010 = "Não foi possível abrir para votação.";
}
