package com.sicredi.assembleia.util;

import com.sicredi.assembleia.config.AppConfig;
import com.sicredi.assembleia.dto.BotaoRecordDto;
import com.sicredi.assembleia.dto.InicioRecordDto;
import com.sicredi.assembleia.dto.ItemInicioRecordDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class FormularioApp {

    private final AppConfig appConfig;

    public FormularioApp(AppConfig appConfig) {
        this.appConfig = appConfig;
    }

    public ResponseEntity<InicioRecordDto> selecao(String titulo, List<ItemInicioRecordDto> itens,
                                                   BotaoRecordDto botaoOk,
                                                   BotaoRecordDto botaoRecordDtoCancelar) {
        return formularioBase(appConfig.getFormularioTipoSelecao(), titulo, itens, botaoOk, botaoRecordDtoCancelar);
    }

    public ResponseEntity<InicioRecordDto> formulario(String titulo, List<ItemInicioRecordDto> itens,
                                                      BotaoRecordDto botaoOk,
                                                      BotaoRecordDto botaoRecordDtoCancelar) {
        return formularioBase(appConfig.getFormularioTipoEnvio(), titulo, itens, botaoOk, botaoRecordDtoCancelar);
    }

    private ResponseEntity<InicioRecordDto> formularioBase(String tipoFormulario, String titulo, List<ItemInicioRecordDto> itens,
                                                           BotaoRecordDto botaoOk,
                                                           BotaoRecordDto botaoRecordDtoCancelar) {
        InicioRecordDto inicioRecordDto = new InicioRecordDto(
                tipoFormulario,
                titulo,
                itens,
                botaoOk,
                botaoRecordDtoCancelar
        );

        return ResponseEntity.status(HttpStatus.OK).body(inicioRecordDto);
    }
}