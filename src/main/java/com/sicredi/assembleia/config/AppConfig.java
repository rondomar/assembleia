package com.sicredi.assembleia.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class AppConfig {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Value("${app.url.base}")
    private String baseUrl;

    @Value("${app.url.base.inicio}")
    private String baseUrlInicio;

    @Value("${app.url.base.pauta.listar}")
    private String baseUrlPautaListar;

    @Value("${app.url.base.pauta.cadastrar}")
    private String baseUrlPautaCadastrar;

    @Value("${app.url.base.pauta.detalhar}")
    private String baseUrlPautaDetalhar;

    @Value("${app.url.base.pauta.cadastrar.nova}")
    private String baseUrlPautaCadastrarNova;

    @Value("${app.url.base.votacao.listar}")
    private String baseUrlVotacaoListar;

    @Value("${app.url.base.votacao.cadastrar}")
    private String baseUrlVotacaoCadastrar;

    @Value("${app.url.base.votacao.cadastrar.nova}")
    private String baseUrlVotacaoCadastrarNova;

    @Value("${app.nome.botao.cancelar}")
    private String nomeBotaoCancelar;

    @Value("${app.nome.botao.acessar}")
    private String nomeBotaoAcessar;

    @Value("${app.nome.botao.salvar}")
    private String nomeBotaoSalvar;

    @Value("${app.formulario.tipo.envio}")
    private String formularioTipoEnvio;

    @Value("${app.formulario.tipo.selecao}")
    private String formularioTipoSelecao;

    public String getFormularioTipoEnvio() {
        return formularioTipoEnvio;
    }

    public String getFormularioTipoSelecao() {
        return formularioTipoSelecao;
    }

    public String getNomeBotaoCancelar() {
        return nomeBotaoCancelar;
    }

    public String getNomeBotaoAcessar() {
        return nomeBotaoAcessar;
    }

    public String getNomeBotaoSalvar() {
        return nomeBotaoSalvar;
    }
    public String getBaseUrl() {
        return baseUrl;
    }

    public String getBaseUrlInicio() {
        return this.baseUrl+baseUrlInicio;
    }

    public String getBaseUrlPautaListar() {
        return this.baseUrl+baseUrlPautaListar;
    }

    public String getBaseUrlPautaCadastrar() {
        return this.baseUrl+baseUrlPautaCadastrar;
    }

    public String getBaseUrlPautaDetalhar() {
        return this.baseUrl+ baseUrlPautaDetalhar;
    }

    public String getBaseUrlPautaCadastrarNova() {
        return this.baseUrl+baseUrlPautaCadastrarNova;
    }

    public String getBaseUrlVotacaoListar() {
        return this.baseUrl+baseUrlVotacaoListar;
    }

    public String getBaseUrlVotacaoCadastrar() {
        return this.baseUrl+baseUrlVotacaoCadastrar;
    }

    public String getBaseUrlVotacaoCadastrarNova() {
        return this.baseUrl+baseUrlVotacaoCadastrarNova;
    }
}