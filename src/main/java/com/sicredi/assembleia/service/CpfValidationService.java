package com.sicredi.assembleia.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class CpfValidationService {

    private static final String API_URL = "https://user-info.herokuapp.com/users";
    private final RestTemplate restTemplate;

    @Autowired
    public CpfValidationService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public boolean isValidCpf(String cpf, Boolean validar) {
        String cleanedCpf = cleanCpf(cpf);

        // Verificação básica local do formato do CPF
        if (!isValidFormat(cleanedCpf)) {
            return false;
        }

        if(validar) {

            // Chamada à API externa para validação
            UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(API_URL)
                    .queryParam("cpf", cleanedCpf);

            try {
                ApiResponse response = restTemplate.getForObject(uriBuilder.toUriString(), ApiResponse.class);
                return response != null && response.isValid();
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }

        } else {
            return true;
        }
    }

    private String cleanCpf(String cpf) {
        if (cpf == null) {
            return null;
        }
        return cpf.replaceAll("[^\\d]", "");
    }

    private boolean isValidFormat(String cpf) {
        if (cpf == null || cpf.length() != 11) {
            return false;
        }

        // Implementação de uma verificação básica do CPF
        try {
            int[] cpfArray = cpf.chars().map(Character::getNumericValue).toArray();
            int firstDigit = calculateCpfDigit(cpfArray, 10);
            int secondDigit = calculateCpfDigit(cpfArray, 11);
            return cpfArray[9] == firstDigit && cpfArray[10] == secondDigit;
        } catch (Exception e) {
            return false;
        }
    }

    private int calculateCpfDigit(int[] cpfArray, int factor) {
        int sum = 0;
        for (int i = 0; i < factor - 1; i++) {
            sum += cpfArray[i] * (factor - i);
        }
        int remainder = sum % 11;
        return remainder < 2 ? 0 : 11 - remainder;
    }

    // Classe para mapear a resposta da API
    private static class ApiResponse {
        private boolean valid;

        public boolean isValid() {
            return valid;
        }

        public void setValid(boolean valid) {
            this.valid = valid;
        }
    }
}
