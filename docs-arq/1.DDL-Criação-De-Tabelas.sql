CREATE TABLE tb_associado (
    id_associado UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    nome_associado VARCHAR(255) NOT NULL,
    numero_cpf_associado NUMERIC(11, 0) NOT NULL
);

CREATE TABLE tb_pauta (
    id_pauta UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    nome_pauta VARCHAR(255) NOT NULL,
    descricao TEXT
);

CREATE TABLE tb_votacao (
    id_votacao UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    tempo_da_votacao INTEGER NOT NULL CHECK (tempo_da_votacao BETWEEN 1 AND 60) DEFAULT 1,
    id_associado UUID REFERENCES tb_associado(id_associado),
    id_pauta UUID REFERENCES tb_pauta(id_pauta)
);
