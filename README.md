# Projeto assembleia

## Tecnologias utilizadas
```
Java
Spring boot
PostgreSQL
H2
Junit
Mockito
Postman
Git
Swagger
```

## Diagrama fluxo da aplicação
![](docs-arq/Fluxo-Jornada-Completa.png)
![](docs-arq/Visão-Fluxo-01-02.png)
![](docs-arq/Visão-Fluxo-03.png)
![](docs-arq/Visão-Fluxo-03-04-05.png)
![](docs-arq/Visão-Fluxo-06-07.png)
![](docs-arq/Visão-Fluxo-08.png)

## MER Banco de Dados
![](docs-arq/Diagrama.png)

## DDL e DML
[1.DDL-Criação-De-Tabelas](docs-arq/1.DDL-Criação-De-Tabelas.sql)

[2.DML-Carga-Fake](docs-arq/2.DML-Carga-Fake.sql)
	
## Teste unitário
```
	Junit + Mockito
```

## Postman
![](docs-arq/Passo-01-Sucesso.png)
![](docs-arq/Passo-02-Erro.png)
![](docs-arq/Passo-02-Sucesso.png)
![](docs-arq/Passo-03.png)
![](docs-arq/Passo-03-Erro.png)
![](docs-arq/Passo-03-Sucesso.png)
![](docs-arq/Passo-04-Pauta-Detalhar.png)
![](docs-arq/Passo-04-Sucesso.png)
![](docs-arq/Passo-05-Pauta-Formulario-Cadastro.png)
![](docs-arq/Passo-05-Pauta-Novo-Cadastro.png)
![](docs-arq/Passo-06-Votacao-Cadastrar-Erro.png)
![](docs-arq/Passo-06-Votacao-Cadastrar-Sucesso.png)
![](docs-arq/Passo-06-Votacao-Listar.png)

## Swagger
![](docs-arq/Swagger-UI.png)